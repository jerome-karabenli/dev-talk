const express = require("express");
const ejs = require("ejs");
const app = express();
const PORT = 5000;

// ejs
app.set("views", "../../front/views");
app.set("view engine", "ejs");
app.use(express.static("../../front/public"));

// port
app.listen(PORT, () => {
  console.log(`listening port is ${PORT}`);
});


app.get("/", (req, res) => {
    res.render("maintenance");
  });