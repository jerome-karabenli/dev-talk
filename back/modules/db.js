const mongoose = require("mongoose");

// BDD connexion
mongoose.connect("mongodb://localhost:27017/sub", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// return err if err
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
  console.log("we're connected!");
});

// define bdd schema
const subSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {type: String, required: true, max: 20},
  firstname: {type: String, required: true, max: 20},
  email: {type: String, required: true, max: 30},
  subject: {type: String, max: 200},
  day: {type: Array, required: true, default: undefined},
  date: {type: Date, default: Date.now}
});

// define bdd model
const Subscribers = mongoose.model("sub", subSchema, "list");

module.exports = { subSchema, Subscribers };
