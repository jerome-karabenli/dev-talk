const express = require("express");
const routes = express.Router();
const db = require("./db")
const mongoose = require('mongoose');



// >>>>>>>>>>>>>GET REQUEST<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
// GET racine

routes.get("/", (req, res) => {
  res.render("index");
});

// GET list
routes.get("/list", (req, res, next) => {
  db.Subscribers.find({}, {name:1, firstname:1, email:1, subject:1}, {day:0})
    .exec()
    .then((list) => {
      res.render("list", { list: list });
      // console.log(list);
    })
    .catch(err => {console.log("error: " + err)});
});

// GET modifier
routes.get("/list/m/:id", (req, res) => {
  db.Subscribers.find({ _id: req.params.id })
    .exec()
    .then((list) => {
      res.render("modifier", { list: list });
    })
    .catch((err) => {
      console.log(err);
    });
});

// GET sondage
routes.get("/sondage/:id", (req, res) => {
  db.Subscribers.find({ _id: req.params.id })
    .exec()
    .then((list) => {
      res.render("sondage", { list: list });
    })
    .catch((err) => {
      console.log(err);
    });
});

// >>>>>>>>>>>>>GET API<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
routes.get("/result", (req, res) => {
  db.Subscribers.find({}, {day:1, _id:0})
  .exec()
  .then(result => {
    res.send(result)
  })
  .catch((err) => {console.log(err)})
})


// >>>>>>>>>>>>>POST REQUEST<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
//  POST nouvelle inscription
routes.post("/", (req, res) => {
  try {
    const newSub = new db.Subscribers({
      _id: new mongoose.Types.ObjectId(),
      name: req.body.name,
      firstname: req.body.firstname,
      email: req.body.email,
      subject: req.body.subject,
      day: req.body.day
    });
  
    newSub.save(function (err) {
      if (err) return console.log(err);
    });
    res.redirect("/list");
  } catch (err) {
    console.log("error: " + err)
  }

});


// POST db update sondage
routes.post("/sondage/:id", (req, res) => {
  db.Subscribers.find({ _id: req.params.id })
    .updateOne({
      day: req.body.day
    })
    .exec()
    .then(() => {res.redirect('/list')})
    .catch((err) => console.log(err))
    
});

// POST db update
routes.post("/list/m/:id", (req, res) => {
  db.Subscribers.find({ _id: req.params.id })
    .updateOne({
      name: req.body.name,
      firstname: req.body.firstname,
      email: req.body.email,
      subject: req.body.subject,
    })
    .exec()
    .then(() => {res.redirect('/list')})
    .catch((err) => console.log(err))
});

// POST db delete
routes.post("/list/d/:id", (req, res) => {
  db.Subscribers.deleteOne({ _id: req.params.id })
    .exec()
    .then(() => {
      res.redirect("/list");
    })
    .catch((err) => {
      console.log(err);
    });
});

// >>>>>>>>>>>>>ERROR<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
// All of other routes ==> 404 or 500
routes.use((req, res, next) => {
  const err = new Error("404");
  err.status = 404;
  next(err);
});

routes.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.render("404");
});

module.exports = routes;
