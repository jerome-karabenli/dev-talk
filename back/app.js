const express = require("express");
const ejs = require("ejs");
const app = express();
const routes = require("./modules/routes");
const PORT = 5000;

// ejs
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.set("views", "./front/views");
app.set("view engine", "ejs");
app.use(express.static("./front/public"));

// routes
app.use("/", routes);

// port
app.listen(PORT, () => {
  console.log(`listening port is ${PORT}`);
});
