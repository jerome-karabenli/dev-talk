// >>>>>>>>>>>>CHECKBOX LIMITER<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
let checkBox = document.querySelectorAll('input[name=day]')

for(let i = 0; i < checkBox.length; i++){
    checkBox[i].addEventListener('change', () => {
        let checked = document.querySelectorAll('input[name=day]:checked').length
        if(checked > 2 ){checkBox[i].checked = false}
    })
    
}

// >>>>>>>>>>>>>> API DATA SCRAPPING<<<<<<<<<<<<<<<<<<<<<<<<<
let apiData;
let url = "http://localhost:5000/result"
let option = {method: "GET"}
async function getData() {
    const response = await fetch(url, option);
    const dataJson = await response.json();
    return dataJson
} 

apiData = await getData();

let apiData2;
let apiData3 = [];
for(let days in apiData){
    apiData2 = apiData[days].day
    for(let y in apiData2){
        apiData3.push(apiData2[y])
    }
}

const myarray = ["lundi", "mardi", "mercredi", "jeudi", "vendredi"]
const final = []

const countOccurrences = (arr, val) => arr.reduce((a, v) => (v === val ? a + 1 : a), 0);

for (let i = 0; i < myarray.length; i++)  {
    final.push(countOccurrences(apiData3, myarray[i]))
    }

console.log(final)

// >>>>>>>>>>>>>>>>DARK MODE<<<<<<<<<<<<<<<<<<<<<<<<<<<
let darkModeButton = document.querySelector(".dark-mode-button")
darkModeButton.addEventListener('click', () => {
    if(document.querySelector("main").classList.contains("dark")){
        document.body.className = ""
        localStorage.setItem("dark-mode", "light");
        let darkModeRemover = document.querySelector(".switch-dark-mode")
        darkModeRemover.parentNode.removeChild(darkModeRemover)
    }else{
        darkMode()
    }
})

let darkMode = () => {
    document.querySelector("main").className = "dark";
    localStorage.setItem("dark-mode", "dark")
    let lien_css = document.createElement('link');
        lien_css.href = "../css/dark-mode.css";
        lien_css.rel = "stylesheet";
        lien_css.classList = "switch-dark-mode"
        document.querySelector("head").append(lien_css);
}

if(localStorage.getItem("dark-mode")){
    if(localStorage.getItem("dark-mode") == "dark"){
        darkMode()
    }
}



	
